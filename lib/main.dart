// Copyright 2018 The Flutter team. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'MLFridge',
      home: MyHomePage(title:'My Little Fridge'),
      theme: ThemeData(
        primaryColor: Colors.blueGrey,
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
//  int _counter = 0;
  List<ItemInTheFridge> _fridgeList;
  List<String> _storedFridgeList;

  @override
  void initState() {
    super.initState();
    _loadFridgeList();
  }


  //Loading counter value on start
  _loadFridgeList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _storedFridgeList = (prefs.getStringList('fridge_list') ?? []);
      _fridgeList = _storedFridgeList.map((String jsonString)=>ItemInTheFridge.fromJson(json.decode(jsonString))).toList();
    });
  }

  //Incrementing counter after click
  _addItemToFridgeList(itemInTheFridge) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _fridgeList.add(itemInTheFridge);
      _storedFridgeList = (prefs.getStringList('fridge_list') ?? []);
      _storedFridgeList.add(json.encode(itemInTheFridge.toJson()));

      prefs.setStringList('fridge_list',_storedFridgeList );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: ListView.builder(
            itemCount: _fridgeList.length,
            itemBuilder:(BuildContext context, int index) {
              return Container(
                padding: const EdgeInsets.all(16.0),
                height: 80,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text('${_fridgeList[index].name}'),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget> [
                        Padding(
                          padding: const EdgeInsets.fromLTRB(8.0,0,20,0),
                          child:
                                Text('${_fridgeList[index].obsoleteDate}'.split(' ')[0]),
                        ),
                        Padding(
                          padding: const EdgeInsets.fromLTRB(20,0,8,0),
                          child: Icon(Icons.delete_outline)
                        ),
                      ]
                    ),
                  ],
                )
              );
            }
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _formAddItemToFridge,
        tooltip: 'Add item',
        child: Icon(Icons.add),
      ),
    );
  }


  void _formAddItemToFridge() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          final _formKey = GlobalKey<FormState>();
          String _name;
          DateTime obsoleteDate = DateTime.now();

          // copy from https://stackoverflow.com/questions/52727535/what-is-the-correct-way-to-add-date-picker-in-flutter-app
          Future<Null> _obsoleteDate(BuildContext context) async {
            final DateTime picked = await showDatePicker(
                context: context,
                initialDate: obsoleteDate,
                firstDate: DateTime(2015, 8),
                lastDate: DateTime(2101)
            );
            if (picked != null && picked != obsoleteDate)
              setState(() {
                obsoleteDate = picked;
              });
          }

          return Scaffold(
            appBar: AppBar(
              title: Text('Add a new item to your fridge'),
            ),
            body: Builder(
              builder: (context2) => Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      TextFormField(
                        decoration: const InputDecoration(
                          hintText: 'Name of your food',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter the name of your food';
                          }
                          return null;
                        },
                        onSaved: (value) {
                          _name = value;
                        },
                      ),
                      Text("${obsoleteDate.toLocal()}".split(' ')[0]),
                      SizedBox(height: 20.0,),
                      RaisedButton(
                        onPressed: () => _obsoleteDate(context).then((newObsoleteDate)=>{obsoleteDate=newObsoleteDate}),
                        child: Text('Select date'),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical: 16.0),
                        child: RaisedButton(
                          onPressed: () {
                            // Validate will return true if the form is valid, or false if
                            // the form is invalid.
                            if (_formKey.currentState.validate()) {
                              // Process data.
                              _formKey.currentState.save();
                              ItemInTheFridge itemInTheFridge = new ItemInTheFridge();
                              itemInTheFridge.name = _name;
                              itemInTheFridge.obsoleteDate = obsoleteDate;
                              _addItemToFridgeList(itemInTheFridge);
                              Navigator.pop(context2);
                            }
                          },
                          child: Text('Submit'),
                        ),
                      ),
                    ],
                  )
              ),
            )
          );
        },
      ),
    );
  }
}

class ItemInTheFridge {
  String name;
  DateTime obsoleteDate;

  ItemInTheFridge({this.name, this.obsoleteDate});

  ItemInTheFridge.fromJson(Map<String, dynamic> json)
      : name = json['name'], obsoleteDate = DateTime.parse(json['obsoleteDate']);

  Map<String, dynamic> toJson() => {
    'name': name,
    'obsoleteDate': obsoleteDate.toString()
  };

}












//  Scaffold(
//  appBar: AppBar(
//  title: Text('Generateur de mot anglais'),
//  ),
//  body: Center(
//  child: RandomWords(),


class RandomWordsState extends State<RandomWords> {
  final _suggestions = <WordPair>[];
  final Set<WordPair> _saved = Set<WordPair>();
  final _biggerFont = const TextStyle(fontSize: 18.0);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mon app bar de liste"),
        actions: <Widget>[
          IconButton(icon: Icon(Icons.list),onPressed: _pushSaved,)
        ],
      ),
      // body: ListView(children: _getSavedFood())
      body: _buildSuggestions(),
    );
  }

//  List<Widget> _getSavedFood() {
//    final prefs = SharedPreferences.getInstance();
//    final counter = prefs.getInt('counter') ?? 0;
//  }

  Widget _buildSuggestions(){
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemBuilder: (context,i){
          if (i.isOdd) return Divider();

          final index = i ~/ 2;
          if(index >= _suggestions.length){
            _suggestions.addAll(generateWordPairs().take(10));
          }
          return _buildRow(_suggestions[index]);
        }
    );
  }

  Widget _buildRow(WordPair pair) {
    final bool alreadySaved = _saved.contains(pair);
    return ListTile(
      title: Text(
        pair.asPascalCase,
        style: _biggerFont,
      ),
      trailing: Icon(
        alreadySaved ? Icons.favorite : Icons.favorite_border,
        color: alreadySaved ? Colors.red : null,
      ),
      onTap: (){
        setState(() {
          if(alreadySaved){
            _saved.remove(pair);
          }else{
            _saved.add(pair);
          }
        });
      },
    );
  }


  void _pushSaved() {
    Navigator.of(context).push(
      MaterialPageRoute<void>(
        builder: (BuildContext context) {
          final Iterable<ListTile> tiles = _saved.map(
                (WordPair pair) {
              return ListTile(
                title: Text(
                  pair.asPascalCase,
                  style: _biggerFont,
                ),
              );
            },
          );

          final List<Widget> divided = ListTile
              .divideTiles(
            context: context,
            tiles: tiles,
          ).toList();

          return Scaffold(
            appBar: AppBar(
              title: Text('Saved Suggestions'),
            ),
            body: ListView(children: divided),
          );

        },
      ),
    );
  }

}

class RandomWords extends StatefulWidget {
  @override
  RandomWordsState createState() => RandomWordsState();
}

